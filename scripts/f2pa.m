s = zpk('s');

P1 = 2/(1 + 0.2*s);
P2 = 5/(s*(1 + 5*s));
P = minreal(P1 * P2)

k = 1;
np = 2;
C = k*(1 + 5*s)/(1 + (5/np)*s)

L = minreal(C * P)
[m, p, w] = bode(L);
k = margin(m, p-60, w)

C = k*(1 + 5*s)/(1 + (5/np)*s)
L = minreal(C * P)

[Gm, Pm] = margin(L)
margin(L)