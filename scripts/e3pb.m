%Eddigi munka meghivasa
f2pa

%Eredeti allapotvaltozok
P_zpk = P
P = ss(P);
[A, b, c, d] = ssdata(P)

%Alapadatok megadasa
kszi = 0.7;
T3 = 0.2 / 4;

%Uj polusok felirasa
den = roots([T3*T3, 2*kszi*T3, 1]);
den(3) = -1/T3

%Visszacsatolas elkeszitese
k = acker(A, b, den)
Afb = A - k*b
kr = 1/dcgain(ss(A - k*b, b, c , d))
bfb = b * kr
Pfb = ss(Afb, bfb, c, d);

%Abrazolas
figure(1); step(P)
figure(2); step(Pfb)